const express = require('express');
const app = express();
 
const hbs = require('hbs');
require('./hbs/helpers');

//variables de entorno globales
const port = process.env.PORT || 3000; 


app.use(express.static(__dirname + '/public'))

// Express HBS Engine

hbs.registerPartials(__dirname + '/views/parciales');

app.set('view engine', 'hbs');
 



app.get('/', (req,res) =>{
    res.render('home',{
        nombre: 'Desarrollo Web Avanzado'
    
    });
});

app.get('/about', (req,res) =>{
    res.render('about');
});

app.get('/contacto', (req,res) =>{
    res.render('contacto');
});


app.listen(port, () => {
  console.log(`Escuchando peticiones en puerto ${port}`);
});